package com.primenova.profile.studentprofile.service;

import com.primenova.profile.studentprofile.model.Student;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentService {

    List<Student> studentList = new ArrayList<>();

    public Student addStudent(Student student) {
        studentList.add(student);
        return student;
    }

    public List<Student> getAllStudents() {
        return studentList;
    }

    public Student getStudent(String id) {
        return studentList
                .stream()
                .filter(student -> student.id().equals(id))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Student not found with id "+id+" !"));
    }
}
