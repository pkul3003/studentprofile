package com.primenova.profile.studentprofile.model;

public record Student(String id, String name, String email) {
}
